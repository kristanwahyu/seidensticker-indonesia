<?php

namespace App\Http\Controllers;

use Validator;
use Datatables;
use Response;
use Illuminate\Http\Request;
use App\Model\kemasan;

class kite_kemasan extends Controller
{
    //menyimpan data
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_kemasan'	    => 'required|min:2|max:2',
	        'nama_kemasan'		=> 'required|min:3|alpha',
        ]);

        $input = $request->all();
        if ($validator->passes())
        {
            $kode   = $request->kode_kemasan;
            $nama   = $request->nama_kemasan;
    
            kemasan::create([
                'kite_kode_kemasan'   => $kode,
                'kite_nama_kemasan'        => $nama,
            ]);
    
            return response()->json(['success'=>'1'],200);
        }

        return response()->json(['errors'=>$validator->errors()]);
    }

    //menampilkan data
    public function show()
    {
        $job = kemasan::select('kite_id_kemasan','kite_kode_kemasan','kite_nama_kemasan');
        return Datatables::eloquent($job)->addIndexColumn()->make(true);
    }
    //kirim data json
    public function get()
    {
        return kemasan::all();
    }
    public function getOne($id)
    {
        return kemasan::find($id);
    }
    //datatables
    public function makeDataTable($data)
    {
        return Datatables::eloquent($data)->addIndexColumn()->make(true);
    }
    //edit
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode_kemasan'	    => 'required',
	        'nama_kemasan'		=> 'required',
    	]);
        //validasi ID
        $param = kemasan::find($id);
        if ($param == null) {
            return abort(503);
        }

        kemasan::find($id)->update([
            'kite_kode_kemasan'   => $request->kode_kemasan,
            'kite_nama_kemasan'        => $request->nama_kemasan,
        ]);

        return response()->json(['status'=>'success'],200);

        //APABILA KODE DI GENERATE OTOMATIS kode satker dihapus saja
    }

    //delete
    public function delete($id) {
        $output = kemasan::find($id);
        if($output == null) return abort(503);
        $output->delete();
    }
}
