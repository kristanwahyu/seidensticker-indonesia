<?php

namespace App\Http\Controllers;

use Validator;
use Datatables;
use Response;
use Illuminate\Http\Request;
use App\Model\vendor;
use App\Model\negara;

class kite_vendor extends Controller
{
    //menyimpan data
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'npwp_vendor' => 'required',
            'nama_vendor'		=> 'required',
            'alamat_vendor'	=> 'required',
            'status_vendor' => 'required',
            'negara_vendor' => 'required'
        ]);

        if ($validator->passes())
        {
            $kode   = $request->npwp_vendor;
            $nama   = $request->nama_vendor;
            $alamat = $request->alamat_vendor;
            $negara = $request->negara_vendor;
            $status = $request->status_vendor;
    
            Vendor::create([
                'kite_npwp_vendor'   => $kode,
                'kite_nama_vendor'   => $nama,
                'kite_alamat_vendor' => $alamat,
                'kite_negara_vendor' => $negara,
                'kite_status_vendor' => $status,
                ]);
            return response()->json(['success'=>'1'],200);
        }
        return response()->json(['errors'=>$validator->errors()]);
    }

    //menampilkan data
    public function show()
    {
        $data = vendor::addSelect(['kite_negara_vendor' => negara::select('kite_nama_negara')
        ->whereColumn('kite_id_negara', 'kite_negara_vendor')]);
         return Datatables::eloquent($data)->addIndexColumn()->make(true);
    }

    //kirim data json
    public function getOne($id)
    {
        return vendor::findOrfail($id)
        ->addSelect(['kite_negara_vendor' => negara::select('kite_nama_negara')
        ->whereColumn('kite_id_negara', 'kite_negara_vendor')])->first();
    }
    
    public function negara(Request $request)
    {
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $data = negara::select("kite_id_negara","kite_nama_negara")
            		->where('kite_nama_negara','LIKE',"%$search%")
            		->get();
        }
        return response()->json($data);
   }
    
    //edit
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'npwp_vendor' => 'required',
            'nama_vendor'	=> 'required',
            'alamat_vendor'	=> 'required',
            'status_vendor' => 'required',
            'negara_vendor' => 'required',
    	]);
        //validasi ID
        $param = Vendor::findOrfail($id);
        
        $kode   = $request->npwp_vendor;
        $nama   = $request->nama_vendor;
        $alamat = $request->alamat_vendor;
        $negara = $request->negara_vendor;
        $status = $request->status_vendor;

        $param->update([
            'kite_npwp_vendor'   => $kode,
                'kite_nama_vendor'   => $nama,
                'kite_alamat_vendor' => $alamat,
                'kite_negara_vendor' => $negara,
                'kite_status_vendor' => $status,
        ]);

        return response()->json(['status'=>'success'],200);
    }

    //delete
    public function delete($id) {
        $output = vendor::find($id);
        if($output == null) return abort(503);
        $output->delete();
    }
}
