<?php

namespace App\Http\Controllers;

use Validator;
use Datatables;
use Response;
use Illuminate\Http\Request;
use App\Model\jenis;

class kite_jenis extends Controller
{
    //menyimpan data
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode_jenis'        => 'required',
            'nama_jenis'		=> 'required|min:3',
        ]);

        $input = $request->all();
        if ($validator->passes())
        {
            $nama   = $request->nama_jenis;
    
            jenis::create([
                'kite_kode_jenis'        => $request->kode_jenis,
                'kite_nama_jenis'        => $nama,
            ]);
    
            return response()->json(['success'=>'1'],200);
        }

        return response()->json(['errors'=>$validator->errors()]);
    }

    //menampilkan data
    public function show()
    {
        $job = jenis::select('kite_id_jenis','kite_kode_jenis', 'kite_nama_jenis');
        return Datatables::eloquent($job)->addIndexColumn()->make(true);
    }
    //kirim data json
    public function get()
    {
        return jenis::all();
    }
    public function getOne($id)
    {
        return jenis::find($id);
    }
    //datatables
    public function makeDataTable($data)
    {
        return Datatables::eloquent($data)->addIndexColumn()->make(true);
    }
    

    //delete
    public function delete($id) {
        $output = jenis::find($id);
        if($output == null) return abort(503);
        $output->delete();
    }
}
