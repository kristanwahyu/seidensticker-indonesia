<?php

namespace App\Http\Controllers;

use Validator;
use Datatables;
use Response;
use Illuminate\Http\Request;
use App\Model\motif;

class kite_motif extends Controller
{
    //menyimpan data
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
	        'nama_motif'		=> 'required|min:3|alpha',
        ]);

        $input = $request->all();
        if ($validator->passes())
        {
            $nama   = $request->nama_motif;
    
            motif::create([
                'kite_nama_motif'        => $nama,
            ]);
    
            return response()->json(['success'=>'1'],200);
        }

        return response()->json(['errors'=>$validator->errors()]);
    }

    //menampilkan data
    public function show()
    {
        $job = motif::select('kite_id_motif','kite_nama_motif');
        return Datatables::eloquent($job)->addIndexColumn()->make(true);
    }
    //kirim data json
    public function get()
    {
        return motif::all();
    }
    public function getOne($id)
    {
        return motif::find($id);
    }
    //datatables
    public function makeDataTable($data)
    {
        return Datatables::eloquent($data)->addIndexColumn()->make(true);
    }
    

    //delete
    public function delete($id) {
        $output = motif::find($id);
        if($output == null) return abort(503);
        $output->delete();
    }
}
