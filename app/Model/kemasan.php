<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class kemasan extends Model
{
    //
    protected $primaryKey = 'kite_id_kemasan';
    protected $fillable = ['kite_nama_kemasan','kite_kode_kemasan','deleted_at'];
    protected $guarded = ['updated_at'];
    protected $table = 'kite_tabel_kemasan';
}
