<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    //
    protected $primaryKey = 'kite_id_negara';
    protected $fillable = ['kite_nama_negara','kite_kode_negara','deleted_at'];
    protected $guarded = ['updated_at'];
    protected $table = 'kite_tabel_negara';
   

    public function Vendor(){
        return $this->hasMany(Vendor::class, 'kite_id_vendor', 'kite_negara_vendor');
    }
}
