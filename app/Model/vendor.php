<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $primaryKey = 'kite_id_vendor';
    protected $fillable = ['kite_npwp_vendor', 'kite_nama_vendor', 'kite_alamat_vendor','kite_negara_vendor','kite_status_vendor','deleted_at'];
    protected $guarded = ['updated_at'];
    protected $table = 'kite_tabel_vendor';
    protected $with = ['negara'];

    public function Negara(){
        return $this->belongsTo(Negara::class,'kite_id_negara');
    }
    public function Barang(){
        return $this->hasMany('App\Model\masterbarang', 'kite_vendor_barang');
    }
}
