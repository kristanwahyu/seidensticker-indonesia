<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kite_tabel_barang', function (Blueprint $table) {
            $table->bigIncrements('kite_id_barang');
            $table->bigInteger('kite_kode_barang')->unique();
            $table->string('kite_nama_barang');
            //foreignkeyvendor
            //$table->integer('kite_id_vendor')->unsigned();
            //$table->foreign('kite_id_vendor')->references('kite_id_vendor')->on('kite_tabel_vendor');
            //foreignkeyjenisbarang
            $table->integer('kite_id_jenis')->unsigned();
            $table->foreign('kite_id_jenis')->references('kite_id_jenis')->on('kite_tabel_jenis');
            //foreignkeymotifbarang
            $table->integer('kite_id_motif')->unsigned();
            $table->foreign('kite_id_motif')->references('kite_id_motif')->on('kite_tabel_motif');
            $table->string('kite_artikel_barang');
            $table->string('kite_kode_warna');
            $table->string('kite_nama_warna');
            $table->string('kite_satuan_barang');
            $table->integer('kite_hscode_barang');
            $table->string('kite_status_barang');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
