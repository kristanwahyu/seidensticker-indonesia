@extends('layouts.layout')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@endpush

@section('title', 'Master Vendor')

@section('sidebar')
    @include('sidebar.admin')
@endsection

@section('content')
  {{-- AWAL MAIN CONTENT --}}
<div class="main-content">
    {{-- Breadcrumb --}}
    <div class="content-heading clearfix">
        <div class="heading-left">
            <h1 class="page-title">Vendor</h1>
            <p class="page-subtitle">Fasilitas untuk create, read, dan update master data Vendor</p>
        </div>
        <ul class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Master Data</a></li>
            <li class="active">Vendor</li>
        </ul>
    </div>
    {{-- End Breadcrumb --}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

              	{{-- awal tabel vendor --}}
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Master Vendor</h3>
                    </div>
                    {{-- awal panel body --}}
                    <div class="panel-body">
                        <div class="text-right">
                            <button class="btn btn-primary" data-toggle="modal" href='#modal-tambah'  id="new-vendor"><i class="fa fa-plus"></i> Tambah</button>
                        </div>
                        <br>
                        {{-- awal pembungkus tabel satuan kerja --}}
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped" id="myTable">

                            </table>
                        </div> {{-- akhir pembungkus tabel satuan kerja --}}
                    </div> {{-- akhir panel body --}}
                </div> {{-- akhir tabel satuan kerja --}}

            </div> {{-- End col-md-12 --}}
        </div> {{-- End Row --}}
    </div> {{-- End Container Fluid --}}

</div>
{{-- AKHIR MAIN CONTENT --}}

  {{-- AWAL MODAL TAMBAH vendor --}}
  <div class="modal fade" id="modal-tambah">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Tambah Vendor</h4>
              </div>
              <div class="modal-body">
                  <form  class="form-horizontal" role="form" name="validasiTambah" id="formTambah" novalidate>
                    {{ csrf_field() }}
                      <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">NPWP Vendor</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" id="tambah_npwp_vendor" placeholer="Contoh : 00001" name="npwp_vendor" required>
                                      <span class="text-danger">
                                        <strong id="npwp-error"></strong>
                                    </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">Nama Vendor</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" id="tambah_nama_vendor" name="nama_vendor" placeholder="Contoh : PT XXXXXXXXXXX" required>
                                      <span class="text-danger">
                                        <strong id="nama-error"></strong>
                                    </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">Alamat Vendor</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" id="tambah_alamat_vendor" name="alamat_vendor" placeholder="Contoh : jalan bla bla bla" required>
                                      <span class="text-danger">
                                        <strong id="alamat-error"></strong>
                                    </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">Negara Vendor</label>
                                  <div class="col-sm-8">
                                    <select class="form-control" id="tambah_negara_vendor" name="tambah_negara_vendor" data-id="">
                                    </select>
                                    <span class="text-danger">
                                        <strong id="negara-error"></strong>
                                    </span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Status Vendor</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="tambah_status_vendor" nama="tambah_status_vendor">
                                        <option>--- Pilih Salah Satu ---</option>
                                        <option value="PPJK" >PPJK</option>
                                        <option value="Supplier">Supplier</option>
                                        <option value="Importir">Supplier Importir</option>
                                        <option value="Lokal">Supplier Lokal</option>
                                        <option value="Customer">Customer</option>
                                        <option value="Eksportir">Eksportir</option>
                                    </select>                    
                                    <span class="text-danger">
                                        <strong id="status-error"></strong>
                                    </span>
                                </div>
                            </div>
                          </div>
                      </div>
                  </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-simpan">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
          </div>
      </div>
  </div>
  {{-- AKHIR MODAL TAMBAH SATUAN KERJA --}}

  {{-- AWAL MODAL UBAH SATUAN KERJA --}}
  <div class="modal fade" id="modal-ubah">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Ubah vendor</h4>
              </div>
              <div class="modal-body">
                  <form action="" method="POST" class="form-horizontal" role="form" id="formUbah">
                    <div class="row">
                          <div class="col-sm-12">
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">NPWP vendor</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" id="ubah__npwp_vendor" name="npwp_vendor">
                                      <input type="hidden" id="id_binding">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">Nama vendor</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" id="ubah_kite_nama_vendor" name="nama_vendor">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">Alamat vendor</label>
                                  <div class="col-sm-8">
                                      <input type="text" class="form-control" id="ubah_kite_alamat_vendor" name="alamat_vendor">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label">Negara vendor</label>
                                  <div class="col-sm-8">
                                    <select class="form-control" id="ubah_kite_negara_vendor" name="negara_vendor" data-id="">
                                    </select>                                  
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Status vendor</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="ubah_kite_status_vendor" nama="status_vendor">
                                        <option>--- Pilih Salah Satu ---</option>
                                        <option value="PPJK" >PPJK</option>
                                        <option value="Supplier">Supplier</option>
                                        <option value="Importir">Supplier Importir</option>
                                        <option value="Lokal">Supplier Lokal</option>
                                        <option value="Customer">Customer</option>
                                        <option value="Eksportir">Eksportir</option>
                                    </select>                                
                                </div>
                            </div>
                          </div>
                      </div>
                  </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-ubah-simpan">Simpan</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              </div>
          </div>
      </div>
  </div>
  {{-- AKHIR MODAL UBAH SATUAN KERJA --}}
@endsection

@push('script')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

<script>
$(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    'use strict';

    //datatable
    var table = $('#myTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
            type : "GET",
            url : "/vendors/show"
        },
        "columns": [
            {
                title: "NO",
                data: "DT_Row_Index",
                name: "DT_Row_Index",
                orderable: false,
                searchable: false,
                width: "1%"
            },
            {
                title: 'NPWP VENDOR',
                data: 'kite_npwp_vendor',
                defaultContent: "-",
                name: 'kite_npwp_vendor'
            },
            {
                title: 'NAMA VENDOR',
                data: 'kite_nama_vendor',
                defaultContent: "-",
                name: 'kite_nama_vendor'
            },
            {
                title: 'ALAMAT VENDOR',
                data: 'kite_alamat_vendor',
                defaultContent: "-",
                name: 'kite_alamat_vendor'
            },
            {
                title: 'NEGARA VENDOR',
                data: 'kite_negara_vendor',
                defaultContent: "-",
                name: 'kite_negara_vendor'
            },
            {
                title: 'STATUS VENDOR',
                data: 'kite_status_vendor',
                defaultContent: "-",
                name: 'kite_status_vendor'
            },
            {
                title: '<div class="text-center">ACTION</div>',
                data: null,
                name: 'action',
                render: function (data) {
                    var actions = '';
                    actions = `<button class='btn btn-warning btn-sm ubah-vendor' data-toggle='modal' data-id='${data["kite_id_vendor"]}' href='#modal-ubah'><i class='fa fa-pencil'></i> Ubah</button>
                                <button class='btn btn-danger btn-sm hapus-vendor' data-toggle='modal' data-id='${data['kite_id_vendor']}'><i class='fa fa-trash'></i> Hapus</button>`;                    
                    return actions.replace();
                },
                width: "13%",
                orderable: false,
                searchable: false,
            }


        ],
    });

    //generate code
   // $("#new-satker").on('click', function(){
     //   $.get("/satuan-kerja/code-generate", function(data, status){
       //     if(status == 'success'){
         //       $('#tambah_kode_vendor').val(data);
           // }
       // });
   // });
    //sweet

    //select
            $('#tambah_negara_vendor').select2({
                    placeholder: 'Pilih Negara ...',
                    ajax: {
                        url: "/vendors/negara",
                        dataType: 'json',
                        processResults: function (data) {
                            return {
                                results:  $.map(data, function (item) {
                                        return {
                                            text: item.kite_nama_negara,
                                            id: item.kite_id_negara
                                        }
                                    })
                            };
                        },
                        cache: true,
                    },
                });

                $('#ubah_kite_negara_vendor').select2({
                    placeholder: 'Pilih Negara ...',
                    ajax: {
                        url: "/vendors/negara",
                        dataType: 'json',
                        processResults: function (data) {
                            return {
                                results:  $.map(data, function (item) {
                                        return {
                                            text: item.kite_nama_negara,
                                            id: item.kite_id_negara
                                        }
                                    })
                            };
                        },
                        cache: true,
                    },
                });
            


    $("#btn-simpan").click(function(){
            swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Master KPPBC Ini Akan Disimpan ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#00a65a",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm){
            if (isConfirm) {
                var registerForm = $("#formTambah");
                var formData = registerForm.serialize();
                $('#npwp-error').html("");
                $('#nama-error').html("");
                $('#alamat-error').html("");
                $('#negara-error').html("");
                $('#status-error').html("");

                $.ajax({
                    url : "/vendors/store", 
                    type : "POST",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        "npwp_vendor" : $("#tambah_npwp_vendor").val(),
                        "nama_vendor" : $("#tambah_nama_vendor").val(),
                        "alamat_vendor" : $("#tambah_alamat_vendor").val(),
                        "negara_vendor" : $("#tambah_negara_vendor").val(),
                        "status_vendor" : $("#tambah_status_vendor").val(),
                    },
                    success : function(data){
                        console.log(data);
                        if(data.errors){
                            setTimeout(function(){
                            swal("Gagal", "Data Gagal Disimpan", "error");
                            }, 1000);

                            if(data.errors.npwp_vendor){
                                $('#kode-error').html(data.errors.npwp_vendor[0]);
                            }
                            if(data.errors.nama_vendor){
                                $('#nama-error').html(data.errors.nama_vendor[0]);
                            }
                            if(data.errors.alamat_vendor){
                                $('#alamat-error').html(data.errors.alamat_vendor[0]);
                            }
                            if(data.errors.negara_vendor){
                                $('#negara-error').html(data.errors.negara_vendor[0]);
                            }
                            if(data.errors.status_vendor){
                                $('#status-error').html(data.errors.status_vendor[0]);
                            }
                            $('#modal-tambah').modal('show');
                        }
                        if(data.success){
                            setTimeout(function(){
                                swal({
                                    title: "Sukses",
                                    text: "Data Tersimpan!",
                                    type: "success"
                                    },
                                    function(){
                                        table.ajax.reload();
                                    });
                                }, 1000);
                                $('#modal-tambah').modal('hide');
                        }
                    },
                });
            } else {
            swal('Dibatalkan', 'Data Master KPPBC Batal Simpan :)', 'error');
            $('#modal-tambah').modal('hide');
            }
        });
    });

    //UBAH
    $("#myTable").on('click','.ubah-vendor', function(){
        $.get("/vendors/get/"+$(this).data('id'), function(data, status){
            if(status == 'success'){
                console.log(data);
                $("#ubah_npwp_vendor").val(data['kite_npwp_vendor']);
                $("#ubah_kite_nama_vendor").val(data['kite_nama_vendor']);
                $("#ubah_kite_alamat_vendor").val(data['kite_alamat_vendor']);
                $("#ubah_kite_negara_vendor").val(data['kite_negara_vendor']);
                $("#ubah_kite_status_vendor").val(data['kite_status_vendor']);
                $("#id_binding").val(data['kite_id_vendor']);
            }
        });
    });

    $("#btn-ubah-simpan").click(function(){
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Satuan Kerja Ini Akan Diubah ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#00a65a",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : "/vendors/update/"+$("#id_binding").val(),
                    type : "PUT",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        "npwp_vendor" : $("#ubah_npwp_vendor").val(),
                        "nama_vendor" : $("#ubah_kite_nama_vendor").val(),
                        "alamat_vendor" : $("#ubah_kite_alamat_vendor").val(),
                        "negara_vendor" : $("#ubah_kite_negara_vendor").val(),
                        "status_vendor" : $("#ubah_kite_status_vendor").val(),
                    },
                    success : function(data, status){
                        console.log(data);
                        if(status=="success"){
                            setTimeout(function(){
                                swal({
                                    title: "Sukses",
                                    text: "Data Tersimpan!",
                                    type: "success"
                                    },
                                    function(){
                                        table.ajax.reload();
                                    });
                                }, 1000);
                        }
                        $('#modal-ubah').modal('hide');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        setTimeout(function(){
                            swal("Gagal", "Data Gagal Diubah", "error");
                        }, 1000);
                    }
                });
            } else {
            swal('Dibatalkan', 'Data Satuan Kerja Batal Simpan :)', 'error');
                $('#modal-tambah').modal('hide');
            }
        });
    });

    //hapus
    $("#myTable").on('click','.hapus-vendor', function(){
    var data = $(this).data('id');
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Master vendor akan di hapus PERMANEN ! ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "red",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : "/vendors/delete/"+data,
                    type : "DELETE",
                    data : {
                        "_token": "{{ csrf_token() }}"
                    },
                    success : function(data, status){
                        if(status=="success"){
                            setTimeout(function(){
                                swal({
                                    title: "Sukses",
                                    text: "Data Terhapus!",
                                    type: "success"
                                    },
                                    function(){
                                        table.ajax.reload();
                                    });
                                }, 1000);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        setTimeout(function(){
                            swal("Error deleting!", "Please try again", "error");
                        }, 1000);
                    }
                });
            } else {
            swal('Dibatalkan', 'Data tidak dihapus :)', 'error');
            }
        });
});

    $('#modal-tambah').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input[type='text']")
            .val('')
            .end()
    });
});


</script>
@endpush
