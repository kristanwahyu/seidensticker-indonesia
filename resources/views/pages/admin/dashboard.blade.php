@extends('layouts.layout')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@endpush

@section('title', 'Dashboard')

@section('sidebar')
    @include('sidebar.admin')
@endsection

@section('content')
  <div class="main-content">
      <div class="container-fluid">
        <div class="jumbotron">
          <h2>Selamat Datang Di Halaman Dashboard Admin IT Inventory</h2>
          <h2>PT Seidensticker Indonesia</h2>
        </div>
      </div>
  </div>
@endsection

@push('script')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
@endpush
