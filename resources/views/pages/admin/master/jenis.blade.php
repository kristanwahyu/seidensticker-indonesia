@extends('layouts.layout')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@endpush

@section('title', 'Master Jenis Barang')

@section('sidebar')
    @include('sidebar.admin')
@endsection

@section('content')
  {{-- AWAL MAIN CONTENT --}}
<div class="main-content">
    {{-- Breadcrumb --}}
    <div class="content-heading clearfix">
        <div class="heading-left">
            <h1 class="page-title">Jenis Barang</h1>
            <p class="page-subtitle">Fasilitas untuk create, read, dan update master data Jenis Barang</p>
        </div>
        <ul class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#">Master Data</a></li>
            <li class="active">Jenis Barang</li>
        </ul>
    </div>
    {{-- End Breadcrumb --}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                {{-- validate --}}
                {{-- menampilkan error validasi --}}
               

              	{{-- awal tabel Jenis --}}
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Daftar Jenis Barang</h3>
                    </div>
                    {{-- awal panel body --}}
                    <div class="panel-body">
                        <div class="text-right">
                            <button class="btn btn-primary" data-toggle="modal" href='#modal-tambah'  id="new-satker"><i class="fa fa-plus"></i> Tambah</button>
                        </div>
                        <br>
                        {{-- awal pembungkus tabel Jenis kerja --}}
                        <div class="table-responsive">
                            @verbatim
                            <table class="table table-bordered table-condensed table-striped" id="myTable">

                            </table>
                            @endverbatim
                        </div> {{-- akhir pembungkus tabel Jenis Barang --}}
                    </div> {{-- akhir panel body --}}
                </div> {{-- akhir tabel Jenis --}}

            </div> {{-- End col-md-12 --}}
        </div> {{-- End Row --}}
    </div> {{-- End Container Fluid --}}

</div>
{{-- AKHIR MAIN CONTENT --}}

  {{-- AWAL MODAL TAMBAH Motif --}}
  <div class="modal fade" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Jenis Barang</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/jenis/store')}}" name="tambahjenis" method="POST" class="form-horizontal" role="form" id="formTambah" novalidate>
                  {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Kode Jenis</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tambah_kode_jenis" placeholer="Contoh : HK" name="kode_jenis" required>
                                    <span class="text-danger">
                                      <strong id="kode-error"></strong>
                                  </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Jenis</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tambah_nama_jenis" name="nama_jenis" placeholder="Contoh : Drum, Barrel" required>
                                    <span class="text-danger">
                                      <strong id="nama-error"></strong>
                                  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="btn-simpan" data-toggle="modal" data-target="#tambah">Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
  {{-- AKHIR MODAL TAMBAH Motif KERJA --}}

  
  {{-- AKHIR MODAL UBAH Motif KERJA --}}
@endsection

@push('script')
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>



<script>
$(function(){
    'use strict';
    var table = $('#myTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
            type : "GET",
            url : "/jenis/show"
        },
        "columns": [
            {
                title: "NO",
                data: "DT_Row_Index",
                name: "DT_Row_Index",
                orderable: false,
                searchable: false,
                width: "1%"
            },
            {
                title: 'KODE BARANG',
                data: 'kite_kode_jenis',
                defaultContent: "-",
                name: 'kite_kode_jenis'
            },
            {
                title: 'NAMA JENIS BARANG',
                data: 'kite_nama_jenis',
                defaultContent: "-",
                name: 'kite_nama_jenis'
            },
            {
                title: '<div class="text-center">ACTION</div>',
                data: null,
                name: 'action',
                render: function (data) {
                    var actions = '';
                    actions = `<button class='btn btn-danger btn-sm hapus-satker' data-toggle='modal' data-id='${data['kite_id_jenis']}'><i class='fa fa-trash'></i> Hapus</button>`;
                    return actions.replace();
                },
                width: "13%",
                orderable: false,
                searchable: false,
            }
        ],
    });
    


    //generate code
   // $("#new-satker").on('click', function(){
     //   $.get("/Motif-kerja/code-generate", function(data, status){
       //     if(status == 'success'){
         //       $('#tambah_kode_Motif_kerja').val(data);
           // }
       // });
   // });
    //sweet

    $("#btn-simpan").click(function(){
            swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Master Jenis Barang Ini Akan Disimpan ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#00a65a",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm){
            if (isConfirm) {
                var registerForm = $("#formTambah");
                var formData = registerForm.serialize();
                $('#kode-error').html("");
                $('#nama-error').html("");

                $.ajax({
                    url : "/jenis/store",
                    type : "POST",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        "kode_jenis" : $("#tambah_kode_jenis").val(),
                        "nama_jenis" : $("#tambah_nama_jenis").val()
                    },
                    success : function(data){
                        console.log(data);
                        if(data.errors){
                            setTimeout(function(){
                            swal("Gagal", "Data Gagal Disimpan", "error");
                            }, 1000);

                            if(data.errors.kode_jenis){
                                $('#kode-error').html(data.errors.kode_jenis[0]);
                            }
                            if(data.errors.nama_jenis){
                                $('#nama-error').html(data.errors.nama_jenis[0]);
                            }

                            $('#modal-tambah').modal('show');
                        }
                        if(data.success){
                            setTimeout(function(){
                                swal({
                                    title: "Sukses",
                                    text: "Data Tersimpan!",
                                    type: "success"
                                    },
                                    function(){
                                        table.ajax.reload();
                                    });
                                }, 1000);
                                $('#modal-tambah').modal('hide');
                        }
                    },
                });
            } else {
            swal('Dibatalkan', 'Data Master Jenis Barang Batal Simpan :)', 'error');
            $('#modal-tambah').modal('hide');
            }
        });
    });


//hapus
    $("#myTable").on('click','.hapus-satker', function(){
    var data = $(this).data('id');
    swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Jenis Barang akan di hapus PERMANEN ! ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "red",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : "/jenis/delete/"+data,
                    type : "DELETE",
                    data : {
                        "_token": "{{ csrf_token() }}"
                    },
                    success : function(data, status){
                        if(status=="success"){
                            setTimeout(function(){
                                swal({
                                    title: "Sukses",
                                    text: "Data Terhapus!",
                                    type: "success"
                                    },
                                    function(){
                                        table.ajax.reload();
                                    });
                                }, 1000);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        setTimeout(function(){
                            swal("Error deleting!", "Please try again", "error");
                        }, 1000);
                    }
                });
            } else {
            swal('Dibatalkan', 'Data tidak dihapus :)', 'error');
            }
        });
});


    $('#modal-tambah').on('hidden.bs.modal', function (e) {
        $(this)
            .find("input[type='text']")
            .val('')
            .end()
    });
});


</script>
@endpush
